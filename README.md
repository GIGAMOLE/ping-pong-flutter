# Ping-Pong Test App

## About

This is the first Flutter demo application.  
Mainly tested on the Android OS, so there are possible bugs and misbehaving on the iOS.  

> I have tried a lot of 3rd party libraries, some other official approaches to achieve the swipe down to close functionality. If somebody knows how to develop this feature, please contact me and send some code examples. I missed implementing this feature properly due to the [issue in the Flutter library](https://github.com/flutter/flutter/issues/59741).

The main conditions for this test task are:

- Video playing and app theme
- Custom scrolls and gestures
- Advanced list widgets
- Custom routing
 
The task requirements are described [here](https://www.figma.com/file/WRYEgVbNuVgQpNDyFrFD87/PingPong-Test).

## Screenshots

|  |  |
| - | - | - |
| ![](/screens/screen-video.gif) | ![](/screens/screen-1.png) | ![](/screens/screen-2.png) |
| ![](/screens/screen-3.png) | ![](/screens/screen-4.png) | ![](/screens/screen-5.png) |

## Author

[Basil Miller](https://www.linkedin.com/in/gigamole/)  
[+380 50 569 8419](tel:380505698419)  
[gigamole53@gmail.com](mailto:gigamole53@gmail.com)  

