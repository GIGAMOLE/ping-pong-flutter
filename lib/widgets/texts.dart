import 'package:flutter/material.dart';
import 'package:ping_pong_test/res/typography.dart';

/// A utility class that provides the Text widgets
/// according to the App typography.
class AppTexts {
  AppTexts._();

  static Text headline4(ThemeData theme, String text) {
    return Text(
      text,
      style: theme.textTheme.headline4,
      textAlign: TextAlign.center,
    );
  }

  static Text headline5(ThemeData theme, String text) {
    return Text(
      text,
      style: theme.textTheme.headline5,
    );
  }

  static Text headline6(ThemeData theme, String text) {
    return Text(
      text,
      style: theme.textTheme.headline6,
    );
  }

  static Text subtitle1(ThemeData theme, String text) {
    return Text(
      text,
      style: theme.textTheme.subtitle1,
    );
  }

  static Text subtitle2(ThemeData theme, String text) {
    return Text(
      text,
      style: theme.textTheme.subtitle2,
    );
  }

  static Text subtitle3(ThemeData theme, String text) {
    return Text(
      text,
      style: AppTypography.subtitle3(theme),
    );
  }

  static Text body1(ThemeData theme, String text) {
    return Text(
      text,
      style: theme.textTheme.bodyText1,
    );
  }

  static Text body2(ThemeData theme, String text) {
    return Text(
      text,
      style: theme.textTheme.bodyText2,
    );
  }

  static Text button(ThemeData theme, String text) {
    return Text(
      text,
      style: theme.textTheme.button,
    );
  }
}
