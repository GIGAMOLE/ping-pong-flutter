import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ping_pong_test/res/colors.dart';
import 'package:ping_pong_test/res/dimens.dart';
import 'package:ping_pong_test/widgets/texts.dart';

/// A utility class that provides App custom buttons.
class AppButtons {
  AppButtons._();

  static Widget _button(
    ThemeData theme,
    String text,
    Function onPressed,
    Color color,
    double height,
  ) {
    return Container(
      height: height,
      child: FlatButton(
        color: color,
        padding: const EdgeInsets.symmetric(
          horizontal: AppDimens.offset20,
        ),
        onPressed: onPressed,
        child: AppTexts.button(theme, text),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(AppDimens.cornerMax),
        ),
      ),
    );
  }

  static Widget buttonPrimary(
    ThemeData theme,
    String text,
    Function onPressed,
  ) {
    return _button(
      theme,
      text,
      onPressed,
      AppColors.primary,
      AppDimens.buttonHeight,
    );
  }

  static Widget buttonSecondary(
    ThemeData theme,
    String text,
    Function onPressed,
  ) {
    return _button(
      theme,
      text,
      onPressed,
      AppColors.secondary,
      AppDimens.buttonHeight,
    );
  }

  static Widget buttonPrimarySmall(
    ThemeData theme,
    String text,
    Function onPressed,
  ) {
    return _button(
      theme,
      text,
      onPressed,
      AppColors.primary,
      AppDimens.buttonSmallHeight,
    );
  }

  static Widget buttonSecondarySmall(
    ThemeData theme,
    String text,
    Function onPressed,
  ) {
    return _button(
      theme,
      text,
      onPressed,
      AppColors.secondary,
      AppDimens.buttonSmallHeight,
    );
  }

  static Widget _iconButton(
    IconData icon,
    Function onPressed,
    Color color,
    Color iconColor,
    double size,
  ) {
    return Container(
      width: size,
      height: size,
      child: FlatButton(
        color: color,
        onPressed: onPressed,
        padding: const EdgeInsets.all(0),
        child: Icon(
          icon,
          size: size * 0.5,
          color: iconColor,
        ),
        shape: const CircleBorder(),
      ),
    );
  }

  static Widget iconButtonSurface(IconData icon, Function onPressed) {
    return _iconButton(
      icon,
      onPressed,
      AppColors.surface,
      AppColors.onSurface,
      AppDimens.buttonHeight,
    );
  }

  static Widget iconButtonSurfaceSmall(IconData icon, Function onPressed) {
    return _iconButton(
      icon,
      onPressed,
      AppColors.surface,
      AppColors.onSurface,
      AppDimens.buttonSmallHeight,
    );
  }

  static Widget iconButtonSecondary(IconData icon, Function onPressed) {
    return _iconButton(
      icon,
      onPressed,
      AppColors.secondary,
      AppColors.onSecondary,
      AppDimens.buttonHeight,
    );
  }

  static Widget iconButtonSecondarySmall(IconData icon, Function onPressed) {
    return _iconButton(
      icon,
      onPressed,
      AppColors.secondary,
      AppColors.onSecondary,
      AppDimens.buttonSmallHeight,
    );
  }

  /// Provides a custom widget that is able to change its color
  /// depending on the scroll state.
  static Widget iconButtonSurfaceSmallElevated(
    IconData icon,
    Function onPressed, {
    double percentage = 1.0,
  }) {
    return Container(
      child: _iconButton(
        icon,
        onPressed,
        Color.lerp(AppColors.secondary, AppColors.surface, percentage),
        Color.lerp(AppColors.onSecondary, AppColors.onSurface, percentage),
        AppDimens.buttonSmallHeight,
      ),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: AppColors.shadow,
            blurRadius: AppDimens.shadowBlur,
            offset: Offset(0, percentage * AppDimens.shadowOffset),
          )
        ],
      ),
    );
  }
}
