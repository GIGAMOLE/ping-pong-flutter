import 'package:flutter/material.dart';
import 'package:ping_pong_test/res/colors.dart';
import 'package:ping_pong_test/res/typography.dart';
import 'package:ping_pong_test/ui/action.dart';
import 'package:ping_pong_test/ui/home.dart';
import 'package:ping_pong_test/ui/profile.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: AppColors.background,
      theme: _themeData(),
      initialRoute: HomeScreen.routeName,
      routes: _routes(),
    );
  }

  /// App custom overrided theme.
  ThemeData _themeData() {
    return ThemeData(
      backgroundColor: AppColors.background,
      fontFamily: AppTypography.fontFamily,
      accentColor: AppColors.primary,
      textTheme: AppTypography.textTheme,
    );
  }

  /// App available routes.
  Map<String, WidgetBuilder> _routes() {
    return {
      HomeScreen.routeName: (context) => HomeScreen(),
      ProfileScreen.routeName: (context) => ProfileScreen(),
      ActionScreen.routeName: (context) => ActionScreen(),
    };
  }
}
