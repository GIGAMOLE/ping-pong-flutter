import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ping_pong_test/res/colors.dart';
import 'package:ping_pong_test/res/dimens.dart';
import 'package:ping_pong_test/widgets/buttons.dart';
import 'package:ping_pong_test/widgets/texts.dart';

class ActionScreen extends StatefulWidget {
  static const routeName = "/action";

  @override
  _ActionScreenState createState() => _ActionScreenState();

  static open(BuildContext context, ScreenArguments arguments) {
    Navigator.pushNamed(context, routeName, arguments: arguments);
  }
}

class _ActionScreenState extends State<ActionScreen> {
  ThemeData _theme;

  @override
  Widget build(BuildContext context) {
    _theme = Theme.of(context);

    final ScreenArguments args = ModalRoute.of(context).settings.arguments;

    return Material(
      type: MaterialType.transparency,
      child: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(AppDimens.offset20),
          color: AppColors.primary,
          child: Stack(
            children: [
              AppButtons.iconButtonSurfaceSmallElevated(Icons.close, _close),
              Center(
                child: AppTexts.headline4(_theme, args.title),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _close() => Navigator.pop(context);
}

/// Arguments holder class that contains a title to display.
class ScreenArguments {
  final String title;

  ScreenArguments(this.title);
}
