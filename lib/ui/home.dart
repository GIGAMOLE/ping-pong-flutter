import 'package:flutter/material.dart';
import 'package:ping_pong_test/res/dimens.dart';
import 'package:ping_pong_test/res/strings.dart';
import 'package:ping_pong_test/ui/profile.dart';
import 'package:ping_pong_test/widgets/buttons.dart';
import 'package:ping_pong_test/widgets/texts.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = "/home";

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ThemeData _theme;

  @override
  Widget build(BuildContext context) {
    _theme = Theme.of(context);

    /// Wraps with Material to avoid the yellow lines below the custom font.
    return Material(
      type: MaterialType.transparency,
      child: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            AppTexts.headline4(_theme, AppStrings.homeTitle),
            const SizedBox(height: AppDimens.offset20),
            AppButtons.buttonPrimary(
              _theme,
              AppStrings.profileTitle,
              _openProfile,
            ),
          ],
        ),
      ),
    );
  }

  void _openProfile() => ProfileScreen.open(context);
}
