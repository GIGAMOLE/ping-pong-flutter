import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:ping_pong_test/res/colors.dart';
import 'package:ping_pong_test/res/dimens.dart';
import 'package:ping_pong_test/res/images.dart';
import 'package:ping_pong_test/res/strings.dart';
import 'package:ping_pong_test/res/videos.dart';
import 'package:ping_pong_test/ui/action.dart';
import 'package:ping_pong_test/widgets/buttons.dart';
import 'package:ping_pong_test/widgets/texts.dart';
import 'package:sprintf/sprintf.dart';
import 'package:video_player/video_player.dart';

class ProfileScreen extends StatefulWidget {
  static const routeName = "/profile";

  @override
  _ProfileScreenState createState() => _ProfileScreenState();

  /// This opening the screen as a modal bottom sheet that is able to be
  /// swiped down. Right now its working little bit buggy due to the
  /// custom solution I made.
  static open(BuildContext context) {
    final topPadding = MediaQuery.of(context).padding.top;
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      enableDrag: true,
      backgroundColor: AppColors.transparent,
      shape: ContinuousRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: const Radius.circular(AppDimens.corner16),
        ),
      ),
      builder: (context) {
        return Container(
          margin: EdgeInsets.only(top: topPadding),
          child: ProfileScreen(),
        );
      },
    );
  }
}

class _ProfileScreenState extends State<ProfileScreen>
    with TickerProviderStateMixin {
  /// Video player controller.
  VideoPlayerController _videoPlayerController;

  /// Tabs controller and tabs.
  TabController _tabController;
  List<String> _tabsTitles;
  List<Tab> _tabs;

  /// Scroll controller and its values to check
  /// the available snap and expansion rate.
  ScrollController _scrollController;
  double _collapsingToolbarPercentage;
  bool _isFromEndNotification;
  double _expandedToolbarHeight;
  bool _isScrollPointerUp;
  bool _isToolbarCollapsed;
  bool _isSwipeDownAvailable;

  /// The body header content values to display.
  String _followersCount;
  String _followingCount;
  String _collapsedBodyTxt;
  bool _isProfileBodyTextExpanded;
  TextStyle _expandedBodyTextStyle;

  /// The values for the list and grid after the Sliver header.
  double _listItemWidth;
  double _listItemHeight;
  int _listItemCount;
  int _gridItemCount;

  /// Theme data and media query.
  ThemeData _theme;
  MediaQueryData _mediaQuery;

  @override
  void initState() {
    super.initState();

    _initPlayer();
    _initContent();
    _initTabs();
    _initScroll();
  }

  /// Init video player.
  void _initPlayer() {
    _videoPlayerController = VideoPlayerController.asset(AppVideos.profile)
      ..initialize().then((_) {
        /// To enable the video playback just uncomment the code below.
        /// Turned off to increase the frame rate of the app.

        // _videoPlayerController
        //   ..play()
        //   ..setLooping(true);

        setState(() {});
      });
  }

  /// Init the body content display values.
  void _initContent() {
    _followersCount = Random().nextInt(1000).toString();
    _followingCount = Random().nextInt(1000).toString();

    _isProfileBodyTextExpanded = false;

    _listItemCount = AppImages.items.length * AppDimens.listItemCountMultiplier;
    _gridItemCount = AppImages.items.length * AppDimens.gridItemCountMultiplier;
  }

  /// Init the tabs and controller.
  void _initTabs() {
    const tabsLength = 3;

    _tabsTitles = List<String>.generate(
      tabsLength,

      /// sprintf stands for providing a format string entry.
      (index) => sprintf.call(AppStrings.profileTabFormat, [index + 1]),
      growable: false,
    );
    _tabs = _tabsTitles.map((tabTitle) => Tab(text: tabTitle)).toList();

    _tabController = TabController(length: tabsLength, vsync: this);
  }

  /// Init the main scroll mechanism of the app.
  /// Here is the inner expansion fraction formula and a scroll listener.
  void _initScroll() {
    _isScrollPointerUp = true;
    _isToolbarCollapsed = false;
    _isSwipeDownAvailable = true;
    _isFromEndNotification = false;
    _collapsingToolbarPercentage = 0.0;

    /// The collapsing fraction range.
    const range = RangeValues(
      AppDimens.collapsedToolbarHeight * 1.35,
      AppDimens.collapsedToolbarHeight,
    );

    double _getCollapsingToolbarPercentage(
      double offset,
      double expandedHeight,
    ) {
      return ((offset - expandedHeight + range.start) /
              (range.start - range.end))
          .clamp(0.0, 1.0);
    }

    _scrollController = ScrollController();
    _scrollController.addListener(() {
      setState(() {
        _collapsingToolbarPercentage = _getCollapsingToolbarPercentage(
          _scrollController.offset,
          _expandedToolbarHeight,
        );

        _isToolbarCollapsed = _collapsingToolbarPercentage >= 0.6;
        _isSwipeDownAvailable = _scrollController.offset <= 10;
      });
    });
  }

  /// Setup values that are based on the screen context.
  void _initOnBuildContent(BuildContext context) {
    _theme = Theme.of(context);
    _mediaQuery = MediaQuery.of(context);

    _expandedToolbarHeight =
        _mediaQuery.size.height * AppDimens.expandedToolbarHeightRatio;

    int collapsedBodyCharCount = (_mediaQuery.size.width * 0.22).floor();
    _collapsedBodyTxt =
        AppStrings.profileBody.substring(0, collapsedBodyCharCount) +
            AppStrings.profileBodyEllipsis;
    _expandedBodyTextStyle =
        _theme.textTheme.bodyText1.copyWith(color: AppColors.onBackground);

    _listItemWidth = _mediaQuery.size.width / 2.5;
    _listItemHeight = _listItemWidth * AppDimens.listItemHeightAspectRatio;
  }

  void _back() => Navigator.pop(context);

  void _action(String title) =>
      ActionScreen.open(context, ScreenArguments(title));

  @override
  Widget build(BuildContext context) {
    _initOnBuildContent(context);

    return Material(
      type: MaterialType.transparency,
      child: _buildContainer(context),
    );
  }

  /// Build main container of the app.
  Widget _buildContainer(BuildContext context) {
    return SafeArea(
      /// Clips all the content on the top with rounded corners.
      child: ClipRRect(
        borderRadius: BorderRadius.vertical(
          top: const Radius.circular(AppDimens.corner16),
        ),
        child: Container(
          color: AppColors.background,

          /// Stack that holds body (header and list),
          /// its top toolbar and the close button.
          child: Stack(
            children: [
              _buildBody(),
              _buildCollapsedToolbar(),
              _buildBtnClose(),
            ],
          ),
        ),
      ),
    );
  }

  /// Build main scroll body container.
  Widget _buildBody() {
    /// Listen to the scroll values for snapping toolbar.
    return Listener(
      behavior: HitTestBehavior.opaque,
      onPointerDown: (event) {
        setState(() {
          _isScrollPointerUp = false;
          _isSwipeDownAvailable = false;
        });
      },
      onPointerUp: (event) {
        if (_isScrollPointerUp) return;
        _tryToSnapToCollapsedToolbar();
        setState(() {
          _isFromEndNotification = false;
          _isScrollPointerUp = true;
          _isSwipeDownAvailable = false;
        });
      },
      /// Listening to the snapping case when the scroll is flinging.
      child: NotificationListener<ScrollNotification>(
        onNotification: (notification) {
          if (notification is ScrollEndNotification) {
            if (_isScrollPointerUp && !_isFromEndNotification) {
              _tryToSnapToCollapsedToolbar();
              _isFromEndNotification = true;
            }
          }

          return true;
        },
        child: IgnorePointer(
          ignoring: _isSwipeDownAvailable,
          child: NestedScrollView(
            controller: _scrollController,
            physics: const ClampingScrollPhysics(),
            dragStartBehavior: DragStartBehavior.start,
            headerSliverBuilder: (context, innerBoxIsScrolled) {
              return [
                _buildBodyHeader(context),
              ];
            },
            body: _buildBodyContent(),
          ),
        ),
      ),
    );
  }

  /// It is trying to snap the toolbar when its close to the body content.
  void _tryToSnapToCollapsedToolbar() {
    double snapToCollapsedToolbarOffset =
        _scrollController.position.maxScrollExtent * 0.65;

    if (_scrollController.offset == _scrollController.position.maxScrollExtent)
      return;
    if (_scrollController.offset >= snapToCollapsedToolbarOffset) {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 200),
        curve: Curves.ease,
      );
    }
  }

  /// Scroll to the top of the main scroll.
  void _scrollToTop() {
    _scrollController.animateTo(
      -_scrollController.offset,
      duration: Duration(milliseconds: (_scrollController.offset / 2).floor()),
      curve: Curves.easeInOut,
    );
  }

  /// Build Profile Screen close button.
  Widget _buildBtnClose() {
    return Container(
      margin: const EdgeInsets.all(AppDimens.offset20),
      child: AppButtons.iconButtonSurfaceSmallElevated(
        Icons.arrow_back_ios,
        _back,
        percentage: 1.0 - _collapsingToolbarPercentage,
      ),
    );
  }

  /// Build the collapsed toolbar over the header content when its collapsed.
  Widget _buildCollapsedToolbar() {
    return Opacity(
      opacity: _collapsingToolbarPercentage,
      child: IgnorePointer(
        ignoring: !_isToolbarCollapsed,
        child: GestureDetector(
          /// Prevents from scrolling down the modal.
          onVerticalDragStart: (event) {},
          child: Wrap(children: [
            Container(
              padding: const EdgeInsets.all(AppDimens.offset20),
              color: AppColors.transparent,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      /// Scroll to top on tap.
                      onTap: _scrollToTop,
                      child: AppTexts.headline6(
                        _theme,
                        AppStrings.profileSubtitle,
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: AppButtons.buttonPrimarySmall(
                      _theme,
                      AppStrings.profileBtnAction,
                      () {
                        _action(AppStrings.profileBtnAction);
                      },
                    ),
                  )
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }

  /// Build main scroll header with a custom solution.
  Widget _buildBodyHeader(BuildContext context) {
    return SliverOverlapAbsorber(
      /// Supports the inner and outer scrolls.
      /// Perfectly suited in case of header and tabs.
      handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
      sliver: SliverPersistentHeader(
        delegate: ProfileSliverToolbar(
          _expandedToolbarHeight,
          AppDimens.collapsedToolbarHeight,

          /// The background content of the header.
          Container(
            width: _mediaQuery.size.width,
            color: AppColors.background,
            padding: const EdgeInsets.only(
              // Fix the 1-pixel divider glitch not drawing
              bottom: AppDimens.tabsHeight - 2,
            ),
            child: Stack(
              children: [
                _buildBodyHeaderBackground(),
                _buildBodyHeaderGradient(),
                _buildBodyHeaderContent(),
              ],
            ),
          ),

          /// The dim overlay for the background content.
          IgnorePointer(
            ignoring: !_isToolbarCollapsed,
            child: Container(
              height: _expandedToolbarHeight,
              width: _mediaQuery.size.width,
              color: AppColors.backgroundVariant.withOpacity(
                _collapsingToolbarPercentage,
              ),
            ),
          ),

          /// Tabs.
          _buildBodyTabs(),
        ),
        pinned: true,
        floating: false,
      ),
    );
  }

  /// Main header video container.
  Widget _buildBodyHeaderBackground() {
    var videoSize = _videoPlayerController.value.size;
    var isVideoEnabled = videoSize == null;

    /// Animating the opening when its ready.
    return AnimatedOpacity(
      opacity: isVideoEnabled ? 0 : 1,
      duration: const Duration(seconds: 1),
      child: SizedBox.expand(
        child: FittedBox(
          fit: BoxFit.cover,
          child: SizedBox(
            width: isVideoEnabled ? 0 : videoSize.width,
            height: isVideoEnabled ? 0 : videoSize.height,
            child: VideoPlayer(_videoPlayerController),
          ),
        ),
      ),
    );
  }

  /// Build video overlay gradient.
  Widget _buildBodyHeaderGradient() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: AppDimens.bodyHeaderContentHeight,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: const [AppColors.background, AppColors.transparent],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            stops: const [0.1, 1.0],
          ),
        ),
      ),
    );
  }

  /// Build header content that is placed over the video.
  Widget _buildBodyHeaderContent() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        padding: const EdgeInsets.all(AppDimens.offset20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildBodyHeaderContentTitle(),
            const SizedBox(height: AppDimens.offset12),
            _buildBodyHeaderContentInfo(),
            const SizedBox(height: AppDimens.offset12),
            _buildBodyHeaderContentBody(),
            const SizedBox(height: AppDimens.offset20),
            _buildBodyHeaderContentBottom(),
          ],
        ),
      ),
    );
  }

  /// Build header content title.
  Widget _buildBodyHeaderContentTitle() {
    return AppTexts.headline5(_theme, AppStrings.profileSubtitle);
  }

  /// Build header content subtitle frame.
  Widget _buildBodyHeaderContentInfo() {
    /// Wrap it to make adaptive in case of a short screen.
    return Wrap(
      children: [
        AppTexts.subtitle1(_theme, AppStrings.profileInfoTitle),
        const SizedBox(width: AppDimens.offset12),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            AppTexts.subtitle1(_theme, _followersCount),
            const SizedBox(width: AppDimens.offset4),
            AppTexts.subtitle3(_theme, AppStrings.profileInfoFollowers),
          ],
        ),
        const SizedBox(width: AppDimens.offset12),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            AppTexts.subtitle1(_theme, _followingCount),
            const SizedBox(width: AppDimens.offset4),
            AppTexts.subtitle3(_theme, AppStrings.profileInfoFollowing),
          ],
        ),
      ],
    );
  }

  /// Build the content body expandable text.
  Widget _buildBodyHeaderContentBody() {
    /// Animating the size and its dependent neighbours.
    return AnimatedSize(
      vsync: this,
      duration: const Duration(milliseconds: 200),
      curve: Curves.ease,

      /// Provide the click on all text to give more usability.
      child: GestureDetector(
        onTap: () {
          setState(() {
            _isProfileBodyTextExpanded = !_isProfileBodyTextExpanded;
          });
        },
        behavior: HitTestBehavior.opaque,
        child: Text.rich(
          TextSpan(
            children: <TextSpan>[
              /// Highlight the text when its expanded.
              TextSpan(
                text: _isProfileBodyTextExpanded
                    ? AppStrings.profileBody
                    : _collapsedBodyTxt,
                style: _isProfileBodyTextExpanded
                    ? _expandedBodyTextStyle
                    : _theme.textTheme.bodyText1,
              ),
              TextSpan(
                text: _isProfileBodyTextExpanded
                    ? ""
                    : AppStrings.profileBodyBtnMore,
                style: _theme.textTheme.bodyText2,
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Build header buttons.
  Row _buildBodyHeaderContentBottom() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            AppButtons.buttonPrimary(
              _theme,
              AppStrings.profileBtnAction1,
              () {
                _action(AppStrings.profileBtnAction1);
              },
            ),
            const SizedBox(width: AppDimens.offset8),
            AppButtons.buttonSecondary(
              _theme,
              AppStrings.profileBtnAction2,
              () {
                _action(AppStrings.profileBtnAction2);
              },
            )
          ],
        ),
        AppButtons.iconButtonSecondary(Icons.more_vert, () {}),
      ],
    );
  }

  /// Build header tabs on bottom of it.
  Widget _buildBodyTabs() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
        width: _mediaQuery.size.width,
        padding: const EdgeInsets.only(left: AppDimens.offset6),
        height: AppDimens.tabsHeight,

        /// Provide the custom theme to disable ripple effect.
        child: Theme(
          // Remove the ripple from the tabs
          data: ThemeData(
            highlightColor: AppColors.transparent,
            splashColor: AppColors.transparent,
          ),
          child: Material(
            type: MaterialType.transparency,
            child: TabBar(
              controller: _tabController,
              isScrollable: true,
              tabs: _tabs,
              labelColor: AppColors.primary,
              unselectedLabelColor: AppColors.onBackground,
              indicatorColor: AppColors.transparent,
              labelStyle: _theme.textTheme.subtitle2,
            ),
          ),
        ),
      ),
    );
  }

  /// Provide the body for the scroll.
  Widget _buildBodyContent() {
    return TabBarView(
      controller: _tabController,
      children: _tabsTitles.map(
        (tabTitle) {
          return SafeArea(
            top: false,
            bottom: false,

            /// Constructs the pages with builder to have their own context.
            child: Builder(
              builder: (context) {
                return CustomScrollView(
                  /// Provide the unique page key for the independence.
                  key: PageStorageKey<String>(tabTitle),
                  primary: true,
                  physics: const ClampingScrollPhysics(),
                  shrinkWrap: true,
                  dragStartBehavior: DragStartBehavior.start,
                  slivers: [

                    /// Actually do all the work with the inner scroll.
                    /// It propagates the edge state of the inner to
                    /// the outer scroll.
                    SliverOverlapInjector(
                      handle: NestedScrollView.sliverOverlapAbsorberHandleFor(
                        context,
                      ),
                    ),
                    _buildBodyListHeader(AppStrings.profilePopular),
                    _buildBodyPopularList(),
                    _buildBodyListHeader(AppStrings.profileFeed),
                    _buildBodyFeedList()
                  ],
                );
              },
            ),
          );
        },
      ).toList(),
    );
  }

  /// Build popular list header.
  Widget _buildBodyListHeader(String title) {
    return SliverToBoxAdapter(
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.fromLTRB(
          AppDimens.offset20,
          AppDimens.offset16,
          AppDimens.offset20,
          AppDimens.offset10,
        ),
        child: AppTexts.headline6(_theme, title),
      ),
    );
  }

  /// Build popular list content.
  Widget _buildBodyPopularList() {
    return SliverToBoxAdapter(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: AppDimens.offset4),
        height: _listItemHeight,
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.symmetric(horizontal: AppDimens.offset4),
          itemBuilder: (context, index) {
            return Container(
              width: _listItemWidth,
              height: _listItemHeight,
              child: Image.asset(
                AppImages.items[index % AppImages.items.length],
              ),
            );
          },
          separatorBuilder: (context, index) => const SizedBox(
            width: AppDimens.offset4,
          ),
          itemCount: _listItemCount,
        ),
      ),
    );
  }

  /// Build a feed grid content.
  Widget _buildBodyFeedList() {
    const axisCount = 2;

    return SliverPadding(
      padding: const EdgeInsets.all(AppDimens.offset4),
      sliver: SliverGrid(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: axisCount,
          mainAxisSpacing: AppDimens.offset4,
          crossAxisSpacing: AppDimens.offset4,

          /// Calculated from the sizes of the grid item.
          childAspectRatio: AppDimens.gridItemAspectRatio,
        ),
        delegate: SliverChildBuilderDelegate(
          (context, index) {
            return Image.asset(
              AppImages.items[index % AppImages.items.length],
            );
          },
          childCount: _gridItemCount,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    _scrollController.dispose();
    _tabController.dispose();

    super.dispose();
  }
}

/// Custom Profile toolbar that provides the ability to places content in
/// the own way, dim it, and show only the tab bar in the collapsed state.
class ProfileSliverToolbar extends SliverPersistentHeaderDelegate {
  double _expandedHeight;
  double _collapsedHeight;

  Widget _expandedContent;
  Widget _collapsedOverlay;
  Widget _collapsedContent;

  ProfileSliverToolbar(this._expandedHeight,
      this._collapsedHeight,
      this._expandedContent,
      this._collapsedOverlay,
      this._collapsedContent,);

  @override
  Widget build(BuildContext context,
      double shrinkOffset,
    bool overlapsContent,
  ) {
    return Stack(
      children: [
        Positioned(
          height: _expandedHeight,
          top: -shrinkOffset,
          child: _expandedContent,
        ),
        Center(
          child: _collapsedOverlay,
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: _collapsedContent,
        ),
      ],
    );
  }

  @override
  double get maxExtent => _expandedHeight;

  @override
  double get minExtent => _collapsedHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}
