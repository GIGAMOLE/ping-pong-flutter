import 'package:flutter/material.dart';
import 'package:ping_pong_test/res/colors.dart';

class AppTypography {
  AppTypography._();

  static const fontFamily = "Hoves";

  static const TextStyle _headline4 = TextStyle(
    fontSize: 64,
    height: 1.1,
    letterSpacing: -0.3,
    fontWeight: FontWeight.w700,
    color: AppColors.onBackground,
  );

  static const TextStyle _headline5 = TextStyle(
    fontSize: 40,
    height: 1.1,
    letterSpacing: -0.2,
    fontWeight: FontWeight.w700,
    color: AppColors.onBackground,
  );

  static const TextStyle _headline6 = TextStyle(
    fontSize: 20,
    height: 1.1,
    fontWeight: FontWeight.w700,
    color: AppColors.onBackground,
  );

  static const TextStyle _subtitle1 = TextStyle(
    fontSize: 16,
    height: 1.1,
    fontWeight: FontWeight.w700,
    color: AppColors.onBackground,
  );

  static const TextStyle _subtitle2 = TextStyle(
    fontSize: 16,
    height: 1.1,
    fontWeight: FontWeight.w500,
    color: AppColors.onBackground,
  );

  static TextStyle subtitle3(ThemeData theme) {
    return theme.textTheme.subtitle2.copyWith(
      fontWeight: FontWeight.w400,
    );
  }

  static const TextStyle _body1 = TextStyle(
    fontSize: 14,
    height: 1.5,
    fontWeight: FontWeight.w500,
    color: AppColors.onBackgroundVariant,
  );

  static const TextStyle _body2 = TextStyle(
    fontSize: 14,
    height: 1.5,
    fontWeight: FontWeight.w500,
    color: AppColors.onBackgroundPrimary,
  );

  static const TextStyle _button = TextStyle(
    fontSize: 14,
    height: 1,
    fontWeight: FontWeight.w500,
    color: AppColors.onPrimary,
  );

  static const textTheme = TextTheme(
    headline4: _headline4,
    headline5: _headline5,
    headline6: _headline6,
    subtitle1: _subtitle1,
    subtitle2: _subtitle2,
    bodyText1: _body1,
    bodyText2: _body2,
    button: _button,
  );
}
