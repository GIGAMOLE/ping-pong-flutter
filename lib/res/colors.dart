import 'package:flutter/cupertino.dart';

class AppColors {
  AppColors._();

  static const Color transparent = Color(0x00000000);

  static const Color primary = Color(0xFFF26D34);
  static const Color onPrimary = Color(0xFFFFFFFF);

  static const Color secondary = Color.fromRGBO(69, 69, 69, 0.8);
  static const Color onSecondary = Color(0xFFFFFFFF);

  static const Color background = Color(0xFF000000);
  static const Color backgroundVariant = Color(0xFF1C1C1C);
  static const Color onBackground = Color(0xFFFFFFFF);
  static const Color onBackgroundVariant = Color.fromRGBO(255, 255, 255, 0.7);
  static const Color onBackgroundPrimary = Color(0xFFF26D34);

  static const Color surface = Color(0xFFFFFFFF);
  static const Color onSurface = Color(0xFF000000);

  static const Color shadow = Color.fromRGBO(0, 0, 0, 0.15);
}
