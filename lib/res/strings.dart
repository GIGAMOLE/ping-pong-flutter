class AppStrings {

  AppStrings._();

  // Home
  static const homeTitle = "Home";

  // Profile
  static const profileTitle = "Profile";
  static const profileSubtitle = "Tiger King";
  static const profileInfoTitle = "Joe Exotic";
  static const profileInfoFollowers = "Followers";
  static const profileInfoFollowing = "Following";
  static const profileBody = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
  static const profileBodyEllipsis = "...";
  static const profileBodyBtnMore = "more";
  static const profileBtnAction = "Action";
  static const profileBtnAction1 = "Action 1";
  static const profileBtnAction2 = "Action 2";
  static const profileTabFormat = "Tab %d";
  static const profilePopular = "Popular";
  static const profileFeed = "Feed";
}
