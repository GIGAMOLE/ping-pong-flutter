class AppImages {
  AppImages._();

  static const _item1 = "assets/images/item_1.png";
  static const _item2 = "assets/images/item_2.png";
  static const _item3 = "assets/images/item_3.png";

  static const items = [_item1, _item2, _item3];
}
