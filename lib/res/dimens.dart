class AppDimens {

  AppDimens._();

  // Padding/Margin
  static const double offset2 = 2;
  static const double offset4 = 4;
  static const double offset6 = 6;
  static const double offset8 = 8;
  static const double offset10 = 10;
  static const double offset12 = 12;
  static const double offset16 = 16;
  static const double offset20 = 20;
  static const double offset24 = 24;
  static const double offset32 = 32;

  // Rounded Corner
  static const double corner8 = 8;
  static const double corner16 = 16;
  static const double cornerMax = 999;

  // Button
  static const double buttonSmallHeight = 36;
  static const double buttonHeight = 46;

  // Shadow
  static const double shadowOffset = 5;
  static const double shadowBlur = 15;

  // Profile Screen
  static const double expandedToolbarHeightRatio = 0.8;
  static const double collapsedToolbarHeight = 130;
  static const double bodyHeaderContentHeight = 354;
  static const double tabsHeight = 56;

  static const double listItemHeightAspectRatio = 1.627450980392157;
  static const double gridItemAspectRatio = 0.6606498194945848;

  static const int listItemCountMultiplier = 5;
  static const int gridItemCountMultiplier = 10;
}
